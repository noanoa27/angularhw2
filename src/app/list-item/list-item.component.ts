import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css']
})

export class ListItemComponent implements OnInit {
  @Input() data:any;
  id;
  title;
  price;
  stock;
  color = 'green';

  constructor() { }

  ngOnInit() {
    this.id = this.data.id;
    this.title = this.data.title;
    this.price = this.data.price;
    this.stock = this.data.stock;

    if (this.stock < 10) {
      this.color = 'red';
    }
  }
}